import React from 'react';
import style from './Modal.module.scss';

const Modal = (props) => {

  const closeModal = () => {
    props.cmodal();
  };

  return (
    <div className={style.mcontainer} onClick={() => closeModal()}>
      <div className={style.mcontainer__modal} onClick={e => e.stopPropagation()}>
        <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/clap.png" alt="Clapping hand"/>
        <h2>Good job!</h2>
        <p>Your configurations have been updated successfully.</p>
      </div>
    </div>
  );
};

export default Modal;