import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import PopupMenu from './PopupMenu';
import style from './Navbar.module.scss';

const Navbar = () => {
  const toggleIcons = [
    "https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/hamburger.png",
    "https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/close.png"
  ];
  
  const [toggleMenu, setToggleMenu] = useState(false);

  const toggleHandler = () => {
    setToggleMenu(!toggleMenu);    
  }

  return (
    <header className={style.header}>
      <button className={style.header__menubtn} onClick={() => toggleHandler()}>
        <img src={toggleMenu ? toggleIcons[1] : toggleIcons[0]} alt="Hamburger menu icon" />
      </button>
      <NavLink to="/" className={style.header__logo}>
        <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/logo.png" alt="Logo" />
      </NavLink>
      <button className={style.header__logoutbtn}>
        <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/logout.png" alt="Hamburger menu icon" />
      </button>
      <nav className={style.header__menu}>
        <ul>
          <li>
            <NavLink to="/">Dashboard</NavLink>
          </li>
          <li>
            <NavLink to="/account">Account</NavLink>
          </li>
          <li>
            <NavLink to="/help">Help</NavLink>
          </li>
          <li>
            <NavLink to="/logout">Logout</NavLink>
          </li>
        </ul>
      </nav>
      { toggleMenu && <PopupMenu /> }
    </header>
  );
};

export default Navbar;