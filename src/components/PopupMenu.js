import React from 'react'
import { NavLink } from 'react-router-dom';
import style from './PopupMenu.module.scss';

export default function PopupMenu() {
  return (
    <div className={style.popupmenu}>
      <ul>
        <li>
          <NavLink to="/">Dashboard</NavLink>
        </li>
        <li>
          <NavLink to="/account">Account</NavLink>
        </li>
        <li>
          <NavLink to="/help">Help</NavLink>
        </li>
      </ul>
    </div>
  )
}
