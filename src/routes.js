import React from 'react';
import Account from "./pages/Account";
import Dashboard from "./pages/Dashboard";
import Help from "./pages/Help";
import NoMatch from "./pages/NoMatch";

const routes = [
  {
    path: "/",
    title: "Dashboard",
    exact: true,
    element: <Dashboard />
  },
  {
    path: "/account",
    title: "Account",
    element: <Account />
  },
  {
    path: "/help",
    title: "Help",
    element: <Help />
  },
  {
    path: "*",
    title: "NoMatch",
    element: <NoMatch />
  }
];

export default routes;