import React from 'react';
import Navbar from '../components/Navbar';

const Help = () => {
  return (
    <>
      <Navbar />
      <div className='content'> 
        <h2>Help page</h2>      
      </div>
    </>
  );
};

export default Help;