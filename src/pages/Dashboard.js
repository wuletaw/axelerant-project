import React from 'react';
import Navbar from '../components/Navbar';

const Dashboard = () => {
  return (
    <>
      <Navbar />
      <div className='content'> 
        <h2>Dashboard page</h2>
      </div>
    </>
  );
};

export default Dashboard;