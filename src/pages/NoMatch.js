import React from 'react';
import Navbar from '../components/Navbar';

const NoMatch = () => {
  return (
    <>
      <Navbar />
      <div className='content'>
        <h2>NoMatch page</h2>
      </div>
    </>
  );
};

export default NoMatch;