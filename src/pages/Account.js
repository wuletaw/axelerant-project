import React, {useState} from 'react';
import Modal from '../components/Modal';
import Navbar from '../components/Navbar';
import style from './Account.module.scss';

const Account = () => {
  const [details, setDetails] = useState("manual");
  const [modal, setModal] = useState(false);

  const detailsHandler = (value) => {
    setDetails(value);
  }

  const modalHandler = () => {
    setModal(!modal);
  };

  const closeModal = () => {
    setModal(false);
  }

  return (
    <>
      <Navbar />
      <div className='content'>
        {modal && <Modal cmodal={closeModal} />}
        <div className={style.topsection}>  
          <div className={style.topsection__image}>
            <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/profile.png" alt="Person" />
          </div>
          <form className={style.topsection__form}>
            <h1>MY ACCOUNT</h1>
            <div className={`${style.topsection__form__inputgroup} ${style.topsection__form__name}`}>
              <span>Name</span>
              <input type="text" defaultValue="John Doe" />
            </div>
            <div className={`${style.topsection__form__inputgroup} ${style.topsection__form__mobile}`}>
              <input type="tel" placeholder='Mobile' />
            </div>
            <div className={style.topsection__form__inputgroup}>
              <span>Email</span>
              <input type="email" defaultValue="john.doe@gmail.com" />
            </div>
          </form>
        </div>
        <hr />
        <div className={style.configuration}>
          <h2>Select Configuration</h2>
          <div className={style.configuration__details}>
            <div className={style.configuration__details__parent}>
              <input type="radio" name="details" value="manual" defaultChecked={details === "manual" ? true : false} onClick={() => detailsHandler("manual")} />
              <details open={details === "manual" ? true : false}>
                <summary>Manual Configuration</summary>
                <div>
                  <h3>Services Access</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed massa leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu varius felis. Integer posuere urna ut mi porta, vel fringilla dolor convallis. Sed laoreet sodales sapien vel vestibulum. Nulla dictum eros ut efficitur porttitor. Mauris dignissim, lectus sit amet euismod pretium, ex purus condimentum erat, eu feugiat nisi ipsum ac eros.</p>
                </div>
              </details>
            </div>
            <div className={style.configuration__details__parent}>
              <input type="radio" name="details" value="semi-auto" onClick={() => detailsHandler("semi-auto")} />
              <details open={details === "semi-auto" ? true : false}>
                <summary>Semi-auto Configuration</summary>
                <div>
                  <h3>Semi-auto Access</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed massa leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu varius felis. Integer posuere urna ut mi porta, vel fringilla dolor convallis. Sed laoreet sodales sapien vel vestibulum. Nulla dictum eros ut efficitur porttitor. Mauris dignissim, lectus sit amet euismod pretium, ex purus condimentum erat, eu feugiat nisi ipsum ac eros.</p>
                </div>
              </details>
            </div>
            <div className={style.configuration__details__parent}>
              <input type="radio" name="details" value="automatic" onClick={() => detailsHandler("automatic")} />
              <details open={details === "automatic" ? true : false}>
                <summary>Automatic Configuration</summary>
                <div>
                  <h3>Services Access</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed massa leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu varius felis. Integer posuere urna ut mi porta, vel fringilla dolor convallis. Sed laoreet sodales sapien vel vestibulum. Nulla dictum eros ut efficitur porttitor. Mauris dignissim, lectus sit amet euismod pretium, ex purus condimentum erat, eu feugiat nisi ipsum ac eros.</p>
                </div>
              </details>
            </div>
          </div>
          <div className={style.configuration__buttons}>
            <button className={style.configuration__buttons__update} onClick={() => modalHandler()}>
              <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/configure.png" alt="change logo" />
              Update Configuration
            </button>
            <button className={style.configuration__buttons__cancel}>
              <img src="https://alvimurtaza.github.io/Interview-Front-end/images/l0-l1-l2-engineer/cancel.png" alt="change logo" />
              Cancel
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Account;